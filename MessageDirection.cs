using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Meebey.SmartIrc4net;
using MySql.Data.MySqlClient;
using Merthsoft.DynamicConfig;
using System.Reflection;

namespace Merthsoft.DecBot3 {
	enum MessageDirection {
		In,
		Out,
		Notification,
        Error,
	}
}
